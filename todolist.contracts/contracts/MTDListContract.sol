pragma solidity 0.5.10;

import "openzeppelin-solidity/contracts/ownership/Ownable.sol";
import "openzeppelin-solidity/contracts/math/SafeMath.sol";

contract MTDListContract is Ownable {

    using SafeMath for uint256;

    string public constant ERROR_ACCESS_DENIED = "ERROR_ACCESS_DENIED";
    string public constant ERROR_WRONG_VALUE = "ERROR_WRONG_VALUE";

    enum Status {
        Created,
        Resolved,
        Removed
    }

    struct Item {
        uint256 id;
        address owner;
        Status status;
        uint256 updatedAt;
        uint256 createdAt;
    }

    Item[] public todoItems;

    // owner => [todoItems.key, todoItems.key...]
    mapping(address => uint256[]) public ownerTodoItems;

    event TodoItemCreated(uint256 itemID);

    event TodoItemUpdated(uint256 itemID);

    function create(uint256 _id) external {
        require(!isOwnerIdExist(_id) == true, "Id has already been taken");

        uint256 itemKey = todoItems.push(
            Item(
                _id,
                msg.sender,
                Status.Created,
                block.timestamp,
                block.timestamp
            )
        ).sub(1);

        ownerTodoItems[msg.sender].push(itemKey);

        emit TodoItemCreated(_id);
    }

    function update(
        uint256 _id,
        Status _status
    )
        external
    {
        require(_status > Status.Created, ERROR_ACCESS_DENIED);
        require(isOwnerIdExist(_id), ERROR_WRONG_VALUE);

        uint256 itemKey = getItemKeyByID(_id);

        require(todoItems[itemKey].owner == msg.sender, ERROR_ACCESS_DENIED);
        require(
            todoItems[itemKey].status == Status.Created,
            ERROR_ACCESS_DENIED
        );

        todoItems[itemKey].status = _status;
        todoItems[itemKey].updatedAt = block.timestamp;

        emit TodoItemUpdated(_id);
    }

    function getOwnerTodoItemKeys()
        external
        view
        returns (uint256[] memory keyList)
    {
        keyList = ownerTodoItems[msg.sender];
    }

    function isOwnerIdExist(uint256 _id) internal view returns (bool) {
        uint256 length = ownerTodoItems[msg.sender].length;
        for (uint256 i = 0; i < length; i++) {
            if (todoItems[ownerTodoItems[msg.sender][i]].id == _id) {
                return true;
            }
        }
    }

    function getItemKeyByID(uint256 _id) internal view returns (uint256) {
        uint256 length = ownerTodoItems[msg.sender].length;
        for (uint256 i = 0; i < length; i++) {
            if (todoItems[ownerTodoItems[msg.sender][i]].id == _id) {
                return ownerTodoItems[msg.sender][i];
            }
        }

        revert(ERROR_WRONG_VALUE);
    }

}