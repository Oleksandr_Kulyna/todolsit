const MTDListContract = artifacts.require("./test/MTDListContractTest.sol");

const Utils = require("./utils");

contract('MTDListContract', function (accounts) {

    let todoInstance;

    const STATUS_CREATED = 0;
    const STATUS_RESOLVED = 1;
    const STATUS_REMOVED = 2;

    beforeEach(async function () {
        todoInstance = await MTDListContract.new();
    });

    it("deploy contract & check create && create && isOwnerIdExist && getOwnerTodoItemKeys", async function () {
        await todoInstance.create(28)
            .then(Utils.receiptShouldSucceed);

        await todoInstance.create(28)
            .then(Utils.receiptShouldFailed)
            .catch(Utils.catchReceiptShouldFailed);
        await todoInstance.create(82)
            .then(Utils.receiptShouldSucceed);

        let todoItem = await todoInstance.todoItems.call(0);
        let ownerTodoItems = await todoInstance.getOwnerTodoItemKeys.call({from: accounts[0]});

        assert.equal(ownerTodoItems.length, 2, "ownerTodoItems length is not equal");
        assert.equal(ownerTodoItems[0], 0, "ownerTodoItems length is not equal");

        assert.equal(todoItem.id, 28, "todoItem.id is not equal");
        assert.equal(todoItem.owner, accounts[0], "todoItem.owner is not equal");
        assert.equal(todoItem.status, STATUS_CREATED, "todoItem.status is not equal");
        assert.equal(todoItem.updatedAt > 0, true, "updatedAt is not equal");
        assert.equal(todoItem.createdAt > 0, true, "createdAt is not equal");
    });

    it("update && getItemKeyByID", async function () {
        await todoInstance.create(28)
            .then(Utils.receiptShouldSucceed);

        let todoItem = await todoInstance.todoItems.call(0);
        let ownerTodoItems = await todoInstance.getOwnerTodoItemKeys.call({from: accounts[0]});

        assert.equal(ownerTodoItems.length, 1, "ownerTodoItems length is not equal");
        assert.equal(ownerTodoItems[0], 0, "ownerTodoItems length is not equal");

        let checkUpdatedAt = todoItem.updatedAt;

        assert.equal(todoItem.id, 28, "todoItem.id is not equal");
        assert.equal(todoItem.owner, accounts[0], "todoItem.owner is not equal");
        assert.equal(todoItem.status, STATUS_CREATED, "todoItem.status is not equal");
        assert.equal(todoItem.updatedAt > 0, true, "updatedAt is not equal");
        assert.equal(todoItem.createdAt > 0, true, "createdAt is not equal");


        await todoInstance.update(13, STATUS_CREATED)
            .then(Utils.receiptShouldFailed)
            .catch(Utils.catchReceiptShouldFailed);

        await todoInstance.update(13, STATUS_RESOLVED)
            .then(Utils.receiptShouldFailed)
            .catch(Utils.catchReceiptShouldFailed);

        await todoInstance.update(28, STATUS_RESOLVED, {from: accounts[2]})
            .then(Utils.receiptShouldFailed)
            .catch(Utils.catchReceiptShouldFailed);

        await todoInstance.update(28, STATUS_RESOLVED, {from: accounts[0]})
            .then(Utils.receiptShouldSucceed);

        todoItem = await todoInstance.todoItems.call(0);
        ownerTodoItems = await todoInstance.getOwnerTodoItemKeys.call({from: accounts[0]});

        assert.equal(ownerTodoItems.length, 1, "ownerTodoItems length is not equal");
        assert.equal(ownerTodoItems[0], 0, "ownerTodoItems length is not equal");

        assert.equal(todoItem.id, 28, "todoItem.id is not equal");
        assert.equal(todoItem.owner, accounts[0], "todoItem.owner is not equal");
        assert.equal(todoItem.status, STATUS_RESOLVED, "todoItem.status is not equal");
        assert.equal(todoItem.updatedAt > checkUpdatedAt, true, "updatedAt is not equal");
        assert.equal(todoItem.createdAt > 0, true, "createdAt is not equal");


        await todoInstance.update(28, STATUS_RESOLVED, {from: accounts[0]})
            .then(Utils.receiptShouldFailed)
            .catch(Utils.catchReceiptShouldFailed);
        await todoInstance.update(28, STATUS_REMOVED, {from: accounts[0]})
            .then(Utils.receiptShouldFailed)
            .catch(Utils.catchReceiptShouldFailed);

        await todoInstance.create(82, {from: accounts[1]})
            .then(Utils.receiptShouldSucceed);


        assert.equal(await todoInstance.getItemKeyByIDTest.call(28), 0, "getItemKeyByIDTest is not equal");

        await todoInstance.getItemKeyByIDTest.call(82)
            .then(Utils.receiptShouldFailed)
            .catch(Utils.catchReceiptShouldFailed);
    });

});