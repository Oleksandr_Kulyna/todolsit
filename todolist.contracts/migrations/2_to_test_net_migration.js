const MTDListContract = artifacts.require("./MTDListContract.sol");

const BigNumber = require("bignumber.js");

module.exports = function (deployer, network, accounts) {

    let todoInstance;

    deployer.then(function () {
        return deployer.deploy(MTDListContract);
    }).then(async () => {
        todoInstance = await MTDListContract.deployed();
    }).then(() => {
        console.log("Finished");
        console.log("todoInstance", todoInstance.address);
    }) .catch((err) => {
        console.error("ERROR", err)
    });
};