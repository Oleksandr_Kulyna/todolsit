var Todo = require('./models/todo');

const STATUS_CREATED = 0;
const STATUS_RESOLVED = 1;
const STATUS_REMOVED = 2;

function getTodos(query, res) {
    Todo.find(query, function (err, todos) {
        if (err) {
            res.send(err);
        }

        res.json(todos);
    });
};

module.exports = function (app) {
    // api ---------------------------------------------------------------------
    // get by owner address
    app.get('/api/todos/:owner', function (req, res) {
        getTodos({ owner: req.params.owner }, res);
    });

    // create new todo
    app.post('/api/todos', function (req, res) {
        Todo.create({
            text: req.body.text,
            status: STATUS_CREATED,
            onChainId: req.body.id,
            owner: req.body.owner
        }, function (err, todo) {
            if (err)
                res.send(err);

            getTodos({ owner: todo.owner }, res);
        });

    });

    // update todo status
    app.put('/api/todos/:id/:status', function (req, res) {
        Todo.findOne({onChainId: req.params.id}, async function (err, doc) {
            if (err) {
                res.send(err);
            }
            doc.status = req.params.status;
            await doc.save();

            getTodos({ owner: doc.owner }, res);
        });
    });

    // application -------------------------------------------------------------
    app.get('*', function (req, res) {
        res.sendFile(__dirname + '/public/index.html'); // load the single view file (angular will handle the page changes on the front-end)
    });
};
