var mongoose = require('mongoose');

module.exports = mongoose.model('Todo', {
    text: {
        type: String,
        default: ''
    },
    status: {
        type: Number,
        default: 0
    },
    onChainId: {
        type: Number,
        default: 0
    },
    owner: {
        type: String,
        default: ''
    }
});