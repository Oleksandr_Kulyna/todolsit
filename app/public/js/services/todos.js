angular.module('todoService', [])

// super simple service
// each function returns a promise object
    .factory('Todos', ['$http', function ($http) {
        return {
            get: function (owner) {
                return $http.get('/api/todos/' + owner);
            },
            create: function (todoData) {
                return $http.post('/api/todos', todoData);
            },
            update: function (id, status) {
                return $http.put('/api/todos/' + id + '/' + status);
            }
        }
    }]);