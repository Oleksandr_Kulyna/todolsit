const abi = [
    {
        "constant": false,
        "inputs": [],
        "name": "renounceOwnership",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "constant": true,
        "inputs": [],
        "name": "owner",
        "outputs": [
            {
                "name": "",
                "type": "address"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "constant": true,
        "inputs": [],
        "name": "isOwner",
        "outputs": [
            {
                "name": "",
                "type": "bool"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "constant": true,
        "inputs": [
            {
                "name": "",
                "type": "address"
            },
            {
                "name": "",
                "type": "uint256"
            }
        ],
        "name": "ownerTodoItems",
        "outputs": [
            {
                "name": "",
                "type": "uint256"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "constant": true,
        "inputs": [],
        "name": "ERROR_WRONG_VALUE",
        "outputs": [
            {
                "name": "",
                "type": "string"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "constant": true,
        "inputs": [
            {
                "name": "",
                "type": "uint256"
            }
        ],
        "name": "todoItems",
        "outputs": [
            {
                "name": "id",
                "type": "uint256"
            },
            {
                "name": "owner",
                "type": "address"
            },
            {
                "name": "status",
                "type": "uint8"
            },
            {
                "name": "updatedAt",
                "type": "uint256"
            },
            {
                "name": "createdAt",
                "type": "uint256"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "constant": false,
        "inputs": [
            {
                "name": "newOwner",
                "type": "address"
            }
        ],
        "name": "transferOwnership",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "constant": true,
        "inputs": [],
        "name": "ERROR_ACCESS_DENIED",
        "outputs": [
            {
                "name": "",
                "type": "string"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "anonymous": false,
        "inputs": [
            {
                "indexed": false,
                "name": "onChainID",
                "type": "uint256"
            }
        ],
        "name": "TodoItemCreated",
        "type": "event"
    },
    {
        "anonymous": false,
        "inputs": [
            {
                "indexed": false,
                "name": "onChainID",
                "type": "uint256"
            }
        ],
        "name": "TodoItemUpdated",
        "type": "event"
    },
    {
        "anonymous": false,
        "inputs": [
            {
                "indexed": true,
                "name": "previousOwner",
                "type": "address"
            },
            {
                "indexed": true,
                "name": "newOwner",
                "type": "address"
            }
        ],
        "name": "OwnershipTransferred",
        "type": "event"
    },
    {
        "constant": false,
        "inputs": [
            {
                "name": "_id",
                "type": "uint256"
            }
        ],
        "name": "create",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "constant": false,
        "inputs": [
            {
                "name": "_id",
                "type": "uint256"
            },
            {
                "name": "_status",
                "type": "uint8"
            }
        ],
        "name": "update",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    }
];
const STATUS_RESOLVED = 1;
const STATUS_REMOVED = 2;
const NETWORK_MAINNET = 1;
const contractAddress = "0x5a23B8741DB1036DFB360A92B60449af9d4F7eC6";

angular.module('todoController', [])
    .controller('mainController', ['$scope', '$http', 'Todos', async function ($scope, $http, Todos) {
        $scope.formData = {};
        $scope.loading = true;
        $scope.isAccountUnlocked = false;
        $scope.isMetaMaskExist = ethereum.isMetaMask;
        $scope.statusInfo = {
            isVisible: false,
            status: false,
            message: ''
        };

        if (ethereum.isMetaMask) {
            $scope.isTestNet = ethereum.networkVersion != NETWORK_MAINNET;

            let accounts = await ethereum.enable();
            if (accounts.length > 0) {
                $scope.isAccountUnlocked = true;
            }

            ethereum.on('accountsChanged', function (accounts) {
                window.location.reload()
            });
            ethereum.on('networkChanged', function (networkId) {
                window.location.reload()
            });

        }

        // GET =====================================================================
        Todos.get(web3.eth.accounts[0]).success(function (data) {
            $scope.todos = data;
            $scope.loading = false;
        });

        // CREATE ==================================================================
        $scope.createTodo = function () {
            $scope.loading = true;
            $scope.statusInfo.isVisible = false;
            $scope.statusInfo.message = '';
            
            if (typeof $scope.formData.text == undefined) {
                console.error('error: $scope.formData.text == undefined');
                $scope.loading = false;
            } else {
                web3.eth.getTransactionCount(web3.eth.accounts[0], function(error, nonce) {
                    sendTransaction(
                        web3.eth.contract(abi).at(contractAddress).create.getData(nonce),
                        function (err, result) {
                            $scope.statusInfo.isVisible = true;
                            console.log('1', $scope.statusInfo);
                            if (!err) {
                                Todos.create({
                                    text: $scope.formData.text,
                                    owner: web3.eth.accounts[0],
                                    id: nonce
                                }).success(function (data) {
                                    $scope.loading = false;
                                    $scope.formData = {}; // clear the form so our user is ready to enter another
                                    $scope.todos = data; // assign our new list of todos
                                });
                            }
                        }
                    );
                });
            }
        };

        // DELETE ==================================================================
        $scope.deleteTodo = function (id) {
            $scope.statusInfo.message = '';
            $scope.statusInfo.isVisible = false;
            updateTodo(id, STATUS_REMOVED);
        };

        // COMPLETE ==================================================================
        $scope.completeTodo = function (id) {
            $scope.statusInfo.message = '';
            $scope.statusInfo.isVisible = false;
            console.log(0, $scope.statusInfo);
            updateTodo( id, STATUS_RESOLVED);
        };

        function updateTodo(id, status) {
            $scope.loading = true;

            sendTransaction(
                web3.eth.contract(abi).at(contractAddress).update.getData(id, status),
                function (err, result) {
                    console.log('1', $scope.statusInfo);
                    $scope.statusInfo.isVisible = true;
                    if (!err) {
                        Todos.update(id, status)
                            .success(function (data) {
                                $scope.loading = false;
                                $scope.todos = data;
                            });
                    } else {
                        Todos.get(web3.eth.accounts[0]).success(function (data) {
                            $scope.todos = data;
                            $scope.loading = false;
                        });
                    }
                }
            );

        }

        function sendTransaction(data, callback) {
            ethereum.sendAsync({
                method: 'eth_sendTransaction',
                params: [{
                    to: contractAddress, // Required except during contract publications.
                    from: web3.eth.accounts[0], // must match user's active address.
                    data: data,
                }],
                from: web3.eth.accounts[0],
            }, function (err, result) {
                console.log('0', $scope.statusInfo);
                if (result.result) {
                    waitForReceipt(result.result, callback);
                } else {
                    $scope.statusInfo.message = err;
                }
            });

        }

        function waitForReceipt(hash, callback) {
            web3.eth.getTransactionReceipt(hash, function (err, receipt) {
                $scope.statusInfo.isVisible = true;
                $scope.statusInfo.message = 'Pending transaction. Hash: ' + hash;
                if (err) {
                    $scope.statusInfo.message = err;
                }
                if (receipt !== null) {
                    $scope.statusInfo.status = receipt.status == '0x1';
                    if ($scope.statusInfo.status) {
                        $scope.statusInfo.message = 'Success. Transaction Hash: ' + hash;

                        callback(false, {result: hash});
                    } else {
                        $scope.statusInfo.message = 'Failure. Transaction Hash: ' + hash;

                        callback(true, {result: hash});
                    }
                } else {
                    // Try again in 1 second
                    window.setTimeout(function () {
                        waitForReceipt(hash, callback);
                    }, 1000);
                }
            });
        }

        $scope.getClassByStatus = function (status) {
            switch (status) {
                case STATUS_RESOLVED:
                    return 'success';
                case STATUS_REMOVED:
                    return 'danger';
                default:
                    return 'default';
            }
        };

        $scope.getTextByStatus = function (status) {
            switch (status) {
                case STATUS_RESOLVED:
                    return 'resolved';
                case STATUS_REMOVED:
                    return 'removed';
                default:
                    return 'created';
            }

        };

    }]);

